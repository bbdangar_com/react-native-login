import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {Toolbar} from 'react-native-material-ui';
import { Button } from 'react-native-material-ui';
import {MKTextField} from 'react-native-material-kit';
import Auth0 from 'react-native-auth0';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            accessToken: '',
            email: '',
            name: '',
        }
    }

    render() {

        return (
            <View>
                <Toolbar centerElement="Login" />
                <View style={styles.container}>

                    { this.content() }

                </View>
            </View>
        );
    }

    content() {
        if (this.state.email) {
            return <View style={{ marginTop : 20 }}>
                <Text>Email : {this.state.email}</Text>
                <Text>Name : {this.state.name}</Text>
            </View>
        } else {
            return <Button onPress={() => {

                // init oauth instance
                const auth0 = new Auth0({ domain: 'bbdangar.auth0.com', clientId: 'idoFUn99WnSEgaYuJ9WFsBALinsqqXEe' });

                // attempt login
                auth0
                    .webAuth
                    .authorize({scope: 'openid profile email', audience: 'https://bbdangar.auth0.com/userinfo'})
                    .then((credentials) => {
                        // save access token to state
                        this.setState({
                            accessToken: credentials.accessToken,
                        })
                        console.log(credentials);

                        // get user details
                        auth0.auth
                            .userInfo({ token: credentials.accessToken })
                            .then(data => {

                                // update state
                                this.setState({
                                    email: data.email,
                                    name: data.nickname,
                                })
                                console.log(data);
                            })
                            .catch(err => {
                                console.log("error occurred while trying to get user details: ", err);
                            });
                    }).catch(error => {
                        console.log(error);
                    })
            }} raised primary text="Login" />
        }
    }
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop: 40,
        padding: 20,
        paddingTop: 60,
    },
    textfield: {
        height: 40,
        width: '100%',
        marginTop: 10,
        marginBottom: 10,
    }
});
