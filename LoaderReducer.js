import { LOADER_SHOW, LOADER_HIDE } from '../actions/types';

const INITIAL_STATE = {
    visible : false,
    text : 'please wait'
}

export default (state = INITIAL_STATE, action) => {
	switch(action.type) {

        case LOADER_SHOW :
            return { ...state, visible: true };

        case LOADER_HIDE :
            return { ...state, visible: false };

		default:
            return state;
	}
}
